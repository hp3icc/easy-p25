# Easy-P25
 
P25Reflecto with Dashboard and P25 < Bridge > XLX/DMR


# Install

bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/easy-p25/-/raw/main/install.sh)"

# Source

* P25Reflector : https://github.com/nostar/DVReflectors

* P25Rflector-Dasboard : https://github.com/ShaYmez/P25Reflector-Dashboard

* P252XLX : https://github.com/yl3im/p25-to-xlx

* P252DMR : https://www.lucifernet.com/2020/01/08/build-a-p25-dmr-cross-mode-bridge/
