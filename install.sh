#!/bin/bash

# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
apps=("git" "sudo" "curl" "wget" "sed")

# Función para verificar e instalar una aplicación
check_and_install() {
    app=$1
    if ! dpkg -s $app 2>/dev/null | grep -q "Status: install ok installed"; then
        echo "$app no está instalado. Instalando..."
        sudo apt-get install -y $app
        echo "$app instalado correctamente."
    else
        echo "$app ya está instalado."
    fi
}

# Verificar e instalar cada aplicación
for app in "${apps[@]}"; do
    check_and_install $app
done
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/data-mmdvm.sh)"
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/p25.sh)"
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/p25cross.sh)"
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-p25r)"
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/nginx.sh)"

variable2=$(date +'%Y' | tail -c 5)
sudo sed -i "s/<\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC.*/<\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC © 2018-$variable2\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/easy-p25\/>Script project: Easy-P25<\/a><\/span>/g" /var/www/p25/index.php
sudo sed -i "s/<\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC.*/<\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC © 2018-$variable2\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/easy-p25\/>Script project: Easy-P25<\/a><\/span>/g" /opt/P25Reflector-Dashboard/index.php
menu-p25r
